#!/bin/bash -l
#SBATCH --job-name="run_ring_SSEND_split"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jessica.lamjiahong@uzh.ch
#SBATCH --time=00:10:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=gpu
#SBATCH --hint=nomultithread
#SBATCH --account=uzg2
#SBATCH --output=ring_SSEND_split_output.log

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export CRAY_CUDA_MPS=1

srun ring_SSEND_split
