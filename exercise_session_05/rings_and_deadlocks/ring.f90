!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!!    Skeleton program for Ex. 5.1:
!!    Rings and deadlocks
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
program ring
  
  use mpi

  implicit none
  
  ! Variables declaration
  integer :: my_rank, n_proc, ierror, source_rank, dest_rank, total_sum, left_rank, i

  ! Begin MPI
  call MPI_INIT(ierror)
  
  ! Get my_rank and n_proc
  call MPI_COMM_RANK(MPI_COMM_WORLD, my_rank, ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, n_proc, ierror)

  source_rank = mod(my_rank - 1 + n_proc, n_proc)
  dest_rank = mod(my_rank + 1, n_proc)
  total_sum = 0
  
  do i=0, n_proc
    call MPI_SEND(my_rank, 1, MPI_INT, dest_rank, 1, MPI_COMM_WORLD, ierror)
    call MPI_RECV(left_rank, 1, MPI_INT, source_rank, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)

    total_sum = total_sum + left_rank
  end do

  write(*,*) 'I am processor ', my_rank,' out of ', n_proc, "and the sum is ", total_sum

  call MPI_FINALIZE (ierror)

end program ring
