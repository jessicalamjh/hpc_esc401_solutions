program euler
    use mpi
    implicit none

    integer :: n, n_proc, n_start, n_end, first_start, first_end, my_rank, i, j
    integer :: ierror, max_rank

    integer, parameter :: k32 = selected_int_kind(32)
    integer(kind=k32)  :: total_sum, sum_i, sum_per_proc, prev_end

    integer,parameter :: p32r1 = selected_real_kind(32,1)
    real(kind=p32r1)  :: e, e_i, initial_frac, next_frac, temp_frac

    real(kind=kind(1.d0)) :: zero = 0.0
    real(kind=kind(1.d0)) :: one = 1.0

    n = 1000000000

    call MPI_INIT(ierror)

    call MPI_COMM_RANK(MPI_COMM_WORLD, my_rank, ierror)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, n_proc, ierror)

    max_rank = n_proc - 1

    if (my_rank == 0) then
        total_sum = 0
        do i = 1, n
            total_sum = total_sum + i
        end do

        sum_per_proc = total_sum / n_proc ! workload per proc

        prev_end = 1
        ! first_start = 1

        ! assigning workload to the other processes
        ! sum_i = n_start + (n_start + 1) + ...
        ! stop adding when sum_i >= sum_per_proc, go to next proc
        do i = 0, max_rank
            n_start = prev_end

            sum_i = 0
            do j = n_start, n
                sum_i = sum_i + j

                if (sum_i >= sum_per_proc) then
                    n_end = j
                    if (i == 0) first_end = n_end

                    prev_end = n_end + 1  ! for next process to know where to start from
                    EXIT
                end if
            end do

            if (i == max_rank) n_end = n  ! prevent overshooting n

            ! assign each process their n_start, n_end values
            if (i /= 0) then
                call MPI_SEND(n_start, 1, MPI_INT, i, 1, MPI_COMM_WORLD, ierror)
                call MPI_SEND(n_end, 1, MPI_INT, i, 1, MPI_COMM_WORLD, ierror)
                write(*,*) "Rank 0 has sent the range (", n_start, ", ", n_end, ") to rank ", i
            end if
        end do

        ! correct rank 0's n_start, n_end values
        n_start = first_start
        n_end = first_end
    else
        ! processes of nonzero rank receive their n_start, n_end
        call MPI_RECV(n_start, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
        call MPI_RECV(n_end, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
        write(*,*) "Rank ", my_rank, " has received the range (", n_start, ", ", n_end, ")"
    end if

    ! ensure all processes have reached this point
    call MPI_BARRIER(MPI_COMM_WORLD, ierror)
    if (my_rank == 0) then
        write(*,*) "All processes have received their ranges and are ready to begin calculation"
    end if

    ! calculate the first factorial for each process
    ! initial_frac = 1/(1 * ... * n_start)
    initial_frac = one
    do i = 1, n_start
        temp_frac = initial_frac * (one/i)
        if (temp_frac > zero) then
            initial_frac = temp_frac
        else
            EXIT
        end if
    end do

    write(*,*) "Rank ", my_rank, "has calculated its initial fraction ", initial_frac

    ! calculate the partial term e_i for each process
    e_i = initial_frac
    temp_frac = initial_frac

    do i = (n_start + 1), n_end
        next_frac = temp_frac * (one/i)
        if (next_frac > zero) then
            temp_frac = next_frac
        else
            temp_frac = zero
            EXIT
        end if
        e_i = e_i + temp_frac
    end do

    ! ensure all processes have reached this point
    call MPI_BARRIER(MPI_COMM_WORLD, ierror)
    if (my_rank == 0) write(*,*) "All processes have calculated their partial sum"

    ! collect the partial sums
    if (my_rank == 0) then
        e = e_i
        do i = 1, max_rank
            ! rank 0 receives e_i from rank i
            call MPI_RECV(e_i, 1, MPI_LONG_DOUBLE, i, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
            e = e + e_i
        end do
        write(*,*) "The approximated value of e is ", e
    else
        ! rank i sends e_i to rank 0
        call MPI_SEND(e_i, 1, MPI_LONG_DOUBLE, 0, 1, MPI_COMM_WORLD, ierror)
    end if

end program euler
