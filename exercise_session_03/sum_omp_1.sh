#!/bin/bash -l
#SBATCH --job-name="run_sum_omp"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jessica.lamjiahong@uzh.ch
#SBATCH --time=00:15:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread
#SBATCH --account=uzh8
#SBATCH --output=sum_omp_1_output.log
#SBATCH --error=sum_omp_1_errors.log

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun sum_omp
