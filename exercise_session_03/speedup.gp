set title "OpenMP Speedup"
set xlabel "Number of cpus"
set ylabel "Speedup"
set key top left
plot x t "ideal speedup", "plot_sum_omp.dat" u 1:(2.6232368946075439/$2) w lp t "OpenMP"
