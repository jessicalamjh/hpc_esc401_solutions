#!/bin/bash -l
#SBATCH --job-name="run_cpi_mpi"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jessica.lamjiahong@uzh.ch
#SBATCH --time=00:15:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=36
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=multithread
#SBATCH --account=uzh8
#SBATCH --output=cpi_mpi_output.log
#SBATCH --error=cpi_mpi_errors.log

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun cpi_mpi
