#!/usr/bin/env python 
import os, re

regex = re.compile("(sum_omp_\d+_output.log)")

filelist = {}
for root, dirs, files in os.walk(os.getcwd()):
	for f in files:
		if regex.match(f):
			numCpu = int(re.findall("[0-9]+", f)[0])
			filelist[numCpu] = f

filelist = list(filelist.items())
filelist.sort(key=lambda x: x[0])

for numCpu, f in filelist: 
	_file = open(os.getcwd()+"/"+f, "r")
	output = _file.read()
	_file.close()
	time = float(re.findall("Time taken for the OMP job: (\d+\.\d+)", output, re.M)[0])
	del output

	print("{} {}".format(numCpu, time))
