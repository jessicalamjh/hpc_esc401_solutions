!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!!    Solution for Ex. 5.1:
!!    Rings and deadlocks
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program ring
  
  use mpi

  implicit none
  
  ! Declaration of variables
  integer :: myrank, size_, ierror
  integer :: req, req2
  integer :: left_rank, right_rank, send_buf, recv_buf, sum_, n
  integer, parameter :: tag_right = 1
  integer :: count_ = 1
  
  ! Init MPI environment
  call MPI_INIT(ierror)
  
  call MPI_COMM_RANK(MPI_COMM_WORLD, myrank, ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, size_, ierror)

  ! Compute rank of neighbors
  right_rank = mod(myrank+1, size_)
  left_rank = mod(myrank-1+size_, size_)

  ! Set first sending message: myrank
  send_buf = myrank

  ! Loop over number of processes
  sum_ = 0
  do n = 0, size_-1
    call MPI_ISEND(send_buf, count_, MPI_INTEGER, right_rank, tag_right, MPI_COMM_WORLD, req, ierror)
    call MPI_IRECV(recv_buf, count_, MPI_INTEGER, left_rank, tag_right, MPI_COMM_WORLD, req2, MPI_STATUS_IGNORE, ierror)
    call MPI_WAIT(req, MPI_STATUS_IGNORE, ierror)
    call MPI_WAIT(req2, MPI_STATUS_IGNORE, ierror)

    ! Update sending message and sum up the received rank
    send_buf = recv_buf
    sum_ = sum_ + recv_buf
  end do

  ! Final output
  print*,'I am processor ',myrank,' out of ',size_,' and the sum is ', sum_

  ! Finalize MPI environment
  call MPI_FINALIZE (ierror)

end program ring
