#!/bin/bash
FILE=$1
if [ -z "$FILE" ]; then
	printf "no argument supplied -- exiting \n"
	exit
fi

if [ -f $FILE ]; then
	printf "$FILE exists \n"
else
	printf "$FILE does not exist \n"
	exit
fi

while read LINE; do
	if [ ! -z "$LINE" ]; then
		printf "$LINE \n"
	fi
done < $FILE 
