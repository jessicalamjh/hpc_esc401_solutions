#!/bin/bash
function is_prime(){
	if [ $# -eq 0 ]; then
		local NUM=''
	else
		local NUM=$1
	fi
	while [[ ! $NUM =~ ^[0-9]+$ ]]; do
		printf "input a positive integer \n"
		read NUM
	done	

	if [[ $NUM -eq 1 ]]; then
		printf "$NUM is not a prime number \n"
		exit
	fi

	local SQRT=$(bc <<< "scale=0; sqrt($NUM)")	
	for ((i=2; i<=$SQRT; i++)); do
		if [[ $(($NUM%i)) -eq 0 ]]; then
			printf "$NUM is not a prime number \n"
			exit
		fi
	done
	printf "$NUM is a prime number \n"
}

is_prime $1 >> exercise_4 
