#!/bin/bash
echo "n_threads = 1"
export OMP_NUM_THREADS=1
srun poisson > ex3_omp.log
echo "n_threads = 4"
export OMP_NUM_THREADS=4
srun poisson >> ex3_omp.log
echo "n_threads = 16"
export OMP_NUM_THREADS=16
srun poisson >> ex3_omp.log
echo "n_threads = 36"
export OMP_NUM_THREADS=36
srun poisson >> ex3_omp.log
echo "OMP script is complete"
