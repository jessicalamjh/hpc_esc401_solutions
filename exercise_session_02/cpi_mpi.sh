#!/bin/bash -l
#SBATCH --job-name="run_cpi_mpi"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jessica.lamjiahong@uzh.ch
#SBATCH --time=00:30:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=2
#SBATCH --ntasks-per-node=72
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread
#SBATCH --account=uzh8
#SBATCH --output=cpi_mpi_output.log
#SBATCH --error=cpi_mpi_errors.log

srun cpi_mpi

