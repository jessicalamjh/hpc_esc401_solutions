#!/bin/bash -l
#SBATCH --job-name="run_cpi_omp"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jessica.lamjiahong@uzh.ch
#SBATCH --time=00:05:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=36
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread
#SBATCH --account=uzh8
#SBATCH --output=cpi_omp_output.log
#SBATCH --error=cpi_omp_errors.log

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun cpi_omp
